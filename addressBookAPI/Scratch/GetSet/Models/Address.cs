﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace addressBookAPI.Models
{

    /// </summary>
    public class Address
    {

        /// </summary>
        [Key]
        public long AddressId { get; set; }


        /// </summary>
        [ForeignKey("Person")]
        public long PersonId { get; set; }


        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Street { get; set; }


        /// </summary>
        [Required]
        [MaxLength(50)]
        public string City { get; set; }


        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }


        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Zip { get; set; }



        /// </summary>
        [Required]
        public bool DeleteFlag { get; set; }
    }
}
