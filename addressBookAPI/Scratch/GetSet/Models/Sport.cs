﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace addressBookAPI.Models
{

    /// </summary>
    public class Sport
    {
        /// <summary>
        [Key]
        public long SportId { get; set; }


        /// </summary>
        [ForeignKey("Sports")]
        public long TableTennis { get; set; }

        /// </summary>
        [ForeignKey("Address")]
        public long Chess { get; set; }

     
        /// </summary>
        [Required]
        [MaxLength(15)]
        public string Basketball { get; set; }


        /// </summary>
        [Required]
        [MaxLength(15)]
        public string Badminton { get; set; }


        /// </summary>
        [Required]
        public bool DeleteFlag { get; set; }
    }
}
