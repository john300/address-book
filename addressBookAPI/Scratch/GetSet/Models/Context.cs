﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace addressBookAPI.Models
{
    /// <summary></summary>
    public class AddressBookContext : DbContext
    {
        /// <summary></summary>
        public AddressBookContext(DbContextOptions<AddressBookContext> options)
            : base(options)
        {
        }


        /// </summary>
        public DbSet<Person> Persons { get; set; }


        /// </summary>
        public DbSet<Address> Addresses { get; set; }


        /// </summary>
        public DbSet<Sport> Sports { get; set;  }

        /// </summary>
        public DbSet<Number> Numbers { get; set; }


        }
    }

