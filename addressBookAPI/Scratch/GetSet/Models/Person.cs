﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace addressBookAPI.Models
{

    /// </summary>
    public class Person
    {

        /// </summary>
        [Key]
        public long PersonId { get; set; }


        /// </summary>
        [Required][MaxLength (30)]
        public string FirstName { get; set; }


        /// </summary>
        [Required][MaxLength(30)]
        public string LastName { get; set; }


        /// </summary>
        [Required]
        public DateTime Birthdate { get; set; }


        /// </summary>
        [Required]
        public bool DeleteFlag { get; set; }
    }
}
