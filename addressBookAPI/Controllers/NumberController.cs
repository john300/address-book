﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using addressBookAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace addressBookAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class NumbersController : ControllerBase
    {
        private readonly AddressBookContext NumberContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContext"></param>
        public NumbersController(AddressBookContext pContext) => NumberContext = pContext;
        /// <summary>Gets all numbers</summary>
        // GET api/getNumbers
        [HttpGet]
        public ActionResult GetAllNumbers()
        {
            try
            {
                return Ok(NumberContext.Numbers.OrderBy(a => a.NumberId).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Get number by ID</summary>
        // GET api/getNumberById
        [HttpGet("{id}")]
        public ActionResult<string> GetNumberById(long id)
        {
            try
            {
                return Ok(NumberContext.Numbers.Find(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Add number</summary>
        // POST api/addNumber
        [HttpPost]
        public ActionResult Add([FromBody] Number number)
        {
            try
            {
                NumberContext.Numbers.Add(number);
                NumberContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>Update person</summary>
        // POST api/updateNumber
        [HttpPost("{id}")]
        public ActionResult Update([FromBody] Number number)
        {
            try
            {
                NumberContext.Numbers.Update(number);
                NumberContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
