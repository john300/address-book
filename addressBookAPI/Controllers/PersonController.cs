﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using addressBookAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace addressBookAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly AddressBookContext PersonContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContext"></param>
        public PersonController(AddressBookContext pContext) => PersonContext = pContext;
        /// <summary>Gets all persons</summary>
        // GET api/getPersons
        [HttpGet]
        public ActionResult GetAllPersons()
        {
            try
            {
                return Ok(PersonContext.Persons.OrderBy(a => a.PersonId).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Get person by ID</summary>
        // GET api/getPersonById
        [HttpGet("{id}")]
        public ActionResult<string> GetPersonById(long id)
        {
            try
            {
                return Ok(PersonContext.Persons.Find(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Add person</summary>
        // POST api/addPerson
        [HttpPost]
        public ActionResult Add([FromBody] Person person)
        {
            try
            {
                PersonContext.Persons.Add(person);
                PersonContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>Update person</summary>
        // POST api/updatePerson
        [HttpPost("{id}")]
        public ActionResult Update([FromBody] Person person)
        {
            try
            {
                PersonContext.Persons.Update(person);
                PersonContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
