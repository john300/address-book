﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using addressBookAPI.Models;

namespace addressBookAPI.Controllers
{
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SportsController : Controller
    {


            private readonly AddressBookContext SportContext;

            /// <param name="pContext"></param>
            public SportsController(AddressBookContext pContext) => SportContext = pContext;
            
            /// <summary>Gets all Sports</summary>
            // GET api/getSport
            [HttpGet]
            public ActionResult GetAllSports()
            {
                try
                {
                    return Ok(SportContext.Sports.OrderBy(a => a.SportId).ToList());
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            /// <summary>Get sport by ID</summary>
            // GET api/getSportById
            [HttpGet("{id}")]
            public ActionResult<string> GetSportById(long id)
            {
                try
                {
                    return Ok(SportContext.Sports.Find(id));
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            /// <summary>Add Sports</summary>
            // POST api/addSport
            [HttpPost]
            public ActionResult Add([FromBody] Sport sport)
            {
                try
                {
                    SportContext.Sports.Add(sport);
                    SportContext.SaveChangesAsync();
                    return Ok("Done!");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            /// <summary>Update Sports</summary>
            // POST api/updateSport
            [HttpPost("{id}")]
            public ActionResult Update([FromBody] Sport sport)
            {
                try
                {
                    SportContext.Sports.Update(sport);
                    SportContext.SaveChangesAsync();
                    return Ok("Done!");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }
    }
