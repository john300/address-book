﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using addressBookAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace addressBookAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : ControllerBase
    {
        private readonly AddressBookContext AddressContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pContext"></param>
        public AddressesController(AddressBookContext pContext) => AddressContext = pContext;
        /// <summary>Gets all addresses</summary>
        // GET api/getAddresses
        [HttpGet]
        public ActionResult GetAllAddresses()
        {
            try
            {
                return Ok(AddressContext.Addresses.OrderBy(a => a.AddressId).ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Get address by ID</summary>
        // GET api/getAddressById
        [HttpGet("{id}")]
        public ActionResult<string> GetAddressById(long id)
        {
            try
            {
                return Ok(AddressContext.Addresses.Find(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>Add address</summary>
        // POST api/addAddress
        [HttpPost]
        public ActionResult Add([FromBody] Address address)
        {
            try
            {
                AddressContext.Addresses.Add(address);
                AddressContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>Update person</summary>
        // POST api/updatePerson
        [HttpPost("{id}")]
        public ActionResult Update([FromBody] Address address)
        {
            try
            {
                AddressContext.Addresses.Update(address);
                AddressContext.SaveChangesAsync();
                return Ok("Done!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
