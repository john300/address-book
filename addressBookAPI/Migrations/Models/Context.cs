﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace addressBookAPI.Models
{
    /// <summary></summary>
    public class AddressBookContext : DbContext
    {
        /// <summary></summary>
        public AddressBookContext(DbContextOptions<AddressBookContext> options)
            : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Email> Emails { get; set;  }

        public DbSet<Number> Numbers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .Property(p => p.DeleteFlag)
                .HasDefaultValue(false);

            modelBuilder.Entity<Address>()
                .Property(a => a.DeleteFlag)
                .HasDefaultValue(false);

            modelBuilder.Entity<Email>()
                .Property(a => a.DeleteFlag)
                .HasDefaultValue(false);

            modelBuilder.Entity<Number>()
                .Property(n => n.DeleteFlag)
                .HasDefaultValue(false);
        }
    }
}
