﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace addressBookAPI.Models
{
    public class Email
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long EmailId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey("Email")]
        public long Twitter { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey("Address")]
        public long Instagram { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [MaxLength(15)]
        public string Facebook { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [MaxLength(15)]
        public string Gmail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public bool DeleteFlag { get; set; }
    }
}
